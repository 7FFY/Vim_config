set nocp
execute pathogen#infect()
syntax on
filetype plugin indent on
set encoding=UTF-8
autocmd vimenter * NERDTree
autocmd VimEnter * GitGutter
"set updatetime=300
let NERDTreeShowHidden=1
set number relativenumber
set ruler
set nowrap
set colorcolumn=80
set incsearch
set wildmenu
set ignorecase
set hlsearch
vmap <C-c> "+y"
hi clear SpellBad
hi SpellBad ctermbg=0
hi SpellBad guibg=#ff2929 ctermbg=0
highlight ColorColumn ctermbg=0
set so=7
set encoding=utf-8
set tabstop=4
set shiftwidth=4
set softtabstop=0 noexpandtab
set cursorline
set t_Co=256

let g:airline_powerline_fonts = 1
let g:livepreview_previewer = 'evince'
let g:livepreview_engine = 'pdflatex'
let g:Powerline_symbols = 'fancy'
let g:indentLine_enabled = 1

let g:gruvbox_italic=1
colorscheme molokai
"colorscheme gruvbox
let g:onedark_termcolors=256
"colorscheme onedark
set termguicolors
set background=dark

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

"LaTeX keybindings
autocmd BufNewfile,BufRead *.tex imap -ab \begin{abstract}<ENTER>\end{abstract}
autocmd BufNewfile,BufRead *.tex imap -br \break
autocmd BufNewfile,BufRead *.tex imap -s1 \section*{}<Esc>i
autocmd BufNewfile,BufRead *.tex imap -s2 \subsection*{}
autocmd BufNewfile,BufRead *.tex imap -s3 \subsubsection*{}
autocmd BufNewfile,BufRead *.tex imap -ce \begin{center}<ENTER>\end{center}<Esc>ko
autocmd BufNewfile,BufRead *.tex imap -it \begin{itemize}<ENTER>\end{itemize}<Esc>ko\item<SPACE>item1
autocmd BufNewfile,BufRead *.tex imap -vs \vspace{3mm}
autocmd BufNewfile,BufRead *.tex imap -fl \begin{flushleft}<ENTER>\end{flushleft}<Esc>ko
autocmd BufNewfile,BufRead *.tex imap -bo \textbf{}
autocmd BufNewfile,BufRead *.tex imap -un \underline{}
autocmd BufNewfile,BufRead *.tex imap -al \begin{align*}<ENTER>\end{align*}<Esc>ko
autocmd BufNewfile,BufRead *.tex nmap <C-b> :w<ENTER>:!./lc.sh<ENTER>
autocmd BufNewfile,BufRead *.tex imap -hl \hl{}<Esc>i
autocmd BufNewfile,BufRead *.tex imap -fr \begin{frame}{}<ENTER>\end{frame}<Esc>ko

"Rust keybindings
autocmd BufNewfile,BufRead *.rs nmap <C-b> :w<ENTER>:!cargo run<ENTER>

"General keybindings
nmap <C-j> 14j
nmap <C-k> 14k
"call togglebg#map("<F5>")

"Web dev keybindings
autocmd BufNewfile,BufRead,FileType *.html let b:loaded_delimitMate = 1
autocmd BufNewfile,BufRead *.html imap <Leader>d <div class=""><ENTER><Esc>kf"a
autocmd BufNewfile,BufRead *.html imap <Leader>im <img src="" alt=""><Esc>3bla
autocmd BufNewfile,BufRead *.html imap <Leader>ul <ul><ENTER><Esc>ko<li>
autocmd BufNewfile,BufRead *.html imap <Leader>li <a href="">Link
autocmd BufNewfile,BufRead *.html nmap <C-b> :w<ENTER>:!firefox index.html<ENTER>
autocmd BufNewfile,BufRead *.html imap <Leader>h1 <h1 class=""><Esc>hi
autocmd BufNewfile,BufRead *.html imap <Leader>h2 <h2 class=""><Esc>hi
autocmd BufNewfile,BufRead *.html imap <Leader>cl class=""<Esc>i
autocmd BufNewfile,BufRead *.html imap <Leader>se <section class=""<Esc>i
autocmd BufNewfile,BufRead *.html imap <Leader>fi <figure class=""<Esc>i
autocmd BufNewfile,BufRead *.html imap <Leader>st style=""<Esc>i

set signcolumn=yes
set linebreak
set sidescrolloff=7
set mouse=a
set title
set confirm
let mapleader = "="
let g:ycm_min_num_of_chars_for_completion = 1
let g:ycm_max_num_candidates = 50
let g:ycm_auto_trigger = 1
let g:ycm_always_populate_location_list = 0
let g:ycm_semantic_triggers = {
    \   'css': [ 're!^', 're!^\s+', ': ' ],
    \   'html': [ 're!^', 're!^\s+', ': ' ],
    \ }
let g:syntastic_aggregate_errors = 1
set omnifunc=csscomplete#CompleteCSS
set omnifunc=htmlcomplete#CompleteHTML
set omnifunc=syntaxcomplete#Complete
let g:user_emmet_leader_key="<Leader>"

noremap <silent> <c-u> :call smooth_scroll#up(&scroll, 0, 2)<CR>
noremap <silent> <c-d> :call smooth_scroll#down(&scroll, 0, 2)<CR>
noremap <silent> <c-b> :call smooth_scroll#up(&scroll*2, 0, 4)<CR>
noremap <silent> <c-f> :call smooth_scroll#down(&scroll*2, 0, 4)<CR>
