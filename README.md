# Vim_config

## Plugin list (pathogen)

- delimitmate
- html5.vim
- indentline
- nerdcommenter
- nerdtree
- nerdtree-git-plugin
- rust.vim
- syntastic
- test
- vim-airline
- vim-colorschemes
- vim-css-color
- vim-devicons
- vim-gitgutter
- vim-javascript
- vim-smooth-scroll
- vimtex
- youcompleteme
